﻿using System;
using Live2D.Cubism.Framework.LookAt;
using UnityEngine;

public class CubismLookAtCursor : MonoBehaviour, ICubismLookTarget
{
    public Vector3 GetPosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        return Camera.main.ScreenToViewportPoint(mousePosition) * 2f - Vector3.one;
    }

    public bool IsActive()
    {
        return true;
    }
}
