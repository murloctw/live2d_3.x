﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubismMotionController : MonoBehaviour
{
    public List<string> clipList { get; private set; }

    private void Start()
    {
        this.animationComponent = base.GetComponent<Animation>();
    }

    public void SetClipList(List<string> list)
    {
        this.clipList = list;
    }

    public void CrossFade(string clipName)
    {
        if (this.animationComponent)
        {
            this.animationComponent.CrossFade(clipName);
        }
    }

    public void Play()
    {
        this.animationComponent.Play();
    }

    public void Stop()
    {
        this.animationComponent.Stop();
    }

    private Animation animationComponent;
}

