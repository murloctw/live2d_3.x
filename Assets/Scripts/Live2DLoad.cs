﻿using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using Live2D.Cubism.Framework.Json;
using Live2D.Cubism.Framework;
using Live2D.Cubism.Framework.LookAt;
using Live2D.Cubism.Core;
using System.Runtime.CompilerServices;
using Live2D.Cubism.Rendering;

public class Live2DLoad : MonoBehaviour
{

    [SerializeField]
    private Button LoadButton;
    [SerializeField]
    private bool CrossFade = false;
    [SerializeField]
    private bool UseDefalutPath = false;
    [SerializeField]
    private float fadeLength = 0.3f;
    [SerializeField]
    private string DefalutPath;
    [SerializeField]
    private Vector3 DefalutPos = new Vector3(0f, 1.5f, 0f);
    //[SerializeField]
    //private Transform selectAvatarWindowContent;
    private Animation animation;

    public List<string> motionList = new List<string>();

    private string DesktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

    public void Start()
    {
        LoadButton.onClick.AddListener(OnClickEvent);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            string name = motionList[UnityEngine.Random.Range(0, motionList.Count - 1)];
            Debug.Log("Motion: " + name);
            if (CrossFade)
                animation.CrossFade(name, fadeLength);
            else
                animation.Play(name);
        }
    }

    public void OnClickEvent()
    {
        if (UseDefalutPath)
        {
            if (!string.IsNullOrEmpty(DefalutPath))
                onSuccess(DefalutPath);
        }
        else
        {
            SelectFile();
        }
    }

    //model3.json
    public void SelectFile()
    {
        var extensions = new[] { new ExtensionFilter("Json", "Json"), };

        string translation = "Select Folder";
        this.InitFileBrowser(translation, extensions);
    }

    private void InitFileBrowser(string windowTitle, ExtensionFilter[] filters = null)
    {
        //File
        var paths = StandaloneFileBrowser.OpenFilePanel(windowTitle, DesktopDirectory, filters, false);
        //Folder
        //var paths = StandaloneFileBrowser.OpenFolderPanel(windowTitle, PathCommon.GetDesktopDirectory(), false);

        if (paths.Length == 0)
            return;

        string path = "";
        foreach (var p in paths)
            path += p;

        onSuccess(path);
    }

    private void onSuccess(string path)
    {
        Debug.Log("path: " + path);

        string fileName = Path.GetFileName(path);

        if (fileName.EndsWith(".json") && fileName.Contains(".model"))
            LoadCubism(path);
        else
            Debug.LogError("不支援格式");
    }

    private void LoadCubism(string path)
    {
        CubismModel3Json cubismModel3Json = CubismModel3Json.LoadAtPath(path, BuiltinLoadAssetAtPath);
        if (cubismModel3Json == null)
        {
            Debug.LogError("Cubismデータの読み込みに失敗しました");
            return;
        }
        CubismModel model = cubismModel3Json.ToModel();
        //this.avatarPool.AddCubism(model.gameObject);
        animation = model.gameObject.AddComponent<Animation>();
        string path3 = Path.GetDirectoryName(path) + "/motions/";
        if (Directory.Exists(path3))
        {
            string[] files = Directory.GetFiles(path3, "*.json", SearchOption.AllDirectories);
            List<string> list = new List<string>();
            foreach (string path4 in files)
            {
                CubismMotion3Json cubismMotion3Json = CubismMotion3Json.LoadFrom(File.ReadAllText(path4));
                AnimationClip animationClip = cubismMotion3Json.ToAnimationClip();
                string text = Path.GetFileName(path4).Replace(".motion3.json", string.Empty);
                animationClip.name = text;
                list.Add(text);
                animationClip.wrapMode = WrapMode.Default;
                animationClip.legacy = true;
                animation.AddClip(animationClip, text);
            }
            animation.Play();
            SetClipList(list);
        }

        CubismLookController cubismLookController = model.gameObject.AddComponent<CubismLookController>();
        cubismLookController.Target = FindObjectOfType<CubismLookAtCursor>();
        cubismLookController.BlendMode = CubismParameterBlendMode.Override;
        CubismParameter cubismParameter = model.Parameters.FindById("ParamEyeBallX");
        CubismLookParameter cubismLookParameter = cubismParameter.gameObject.AddComponent<CubismLookParameter>();
        cubismLookParameter.Factor = -1f;
        cubismLookParameter.Axis = CubismLookAxis.X;
        CubismParameter cubismParameter2 = model.Parameters.FindById("ParamEyeBallY");
        CubismLookParameter cubismLookParameter2 = cubismParameter2.gameObject.AddComponent<CubismLookParameter>();
        cubismLookParameter2.Factor = 1f;
        cubismLookParameter2.Axis = CubismLookAxis.Y;
        cubismLookController.Refresh();
        cubismLookController.enabled = false;
        CubismRenderController component = model.GetComponent<CubismRenderController>();
        component.SortingMode = CubismSortingMode.BackToFrontOrder;
        model.gameObject.transform.eulerAngles = new Vector3(0f, 0f, 0f);
        model.gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);
        model.gameObject.transform.position = DefalutPos;
        //GameObject button = Instantiate(avatarButtonPrefab);
        //buttons.Add(button);
        //button.transform.SetParent(this.selectAvatarWindowContent, false);
        //RuntimePreviewGenerator.OrthographicMode = false;
        //RuntimePreviewGenerator.BackgroundColor = Color.white;
        //RuntimePreviewGenerator.PreviewDirection = new Vector3(0f, 0f, -1f);
        //AvatarButton component2 = button.GetComponent<AvatarButton>();
        //Texture2D texture2D = RuntimePreviewGenerator.GenerateModelPreview(model.transform, 256, 256, false);
        //if (texture2D)
        //{
        //    component2.thumb.sprite = Sprite.Create(texture2D, new Rect(0f, 0f, (float)texture2D.width, (float)texture2D.height), Vector2.zero);
        //}
        //string fileName = Path.GetFileName(path);
        //component2.avatarName.text = fileName.Remove(fileName.Length - ".model3.json".Length);
        //component2.formatTitle.text = "Live2D Cubism";
        //component2.avatarButton.onClick.AddListener(delegate ()
        //{
        //    avatarPool.SelectCubismAvatar(model.gameObject);
        //});
        //component2.deleteButton.onClick.AddListener(delegate ()
        //{
        //    DialogManager.Instance.Show(DialogManager.Type.allert, "Destory?", delegate ()
        //    {
        //        Destroy(button);
        //        avatarPool.Remove(model.gameObject);
        //        DeleteCubismFolder(path);
        //    }, delegate ()
        //    {
        //    });
        //});

        //model.gameObject.AddComponent<Live2DApplier>();
    }

    public static object BuiltinLoadAssetAtPath(Type assetType, string absolutePath)
    {
        if (assetType == typeof(byte[]))
        {
            return File.ReadAllBytes(absolutePath);
        }
        else if (assetType == typeof(string))
        {
            return File.ReadAllText(absolutePath);
        }
        else if (assetType == typeof(Texture2D))
        {
            var texture2D = new Texture2D(1, 1);
            texture2D.LoadImage(File.ReadAllBytes(absolutePath));
            return texture2D;
        }
        throw new NotSupportedException();
    }

    private void SetClipList(List<string> list)
    {
        motionList = list;
    }
}
