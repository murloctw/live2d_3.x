﻿using Live2D.Cubism.Core;
using Live2D.Cubism.Framework.Json;
using Live2D.Cubism.Rendering;
using Mitene;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Loader : MonoBehaviour
{
    private void RemoveCubismFolderFromFullPath(string fullPath)
    {
        try
        {
            string directoryName = Path.GetDirectoryName(fullPath);
            Directory.Delete(directoryName, true);
            UnityEngine.Debug.LogError("remove live2D data : " + directoryName);
        }
        catch (IOException ex)
        {
            UnityEngine.Debug.LogError(ex.Message);
        }
        catch (Exception ex2)
        {
            UnityEngine.Debug.LogError(ex2.GetType().FullName + ": " + ex2.Message);
        }
    }

    public static object BuiltinLoadAssetAtPath(Type assetType, string absolutePath)
    {
        if (assetType == typeof(byte[]))
        {
            return File.ReadAllBytes(absolutePath);
        }
        if (assetType == typeof(string))
        {
            return File.ReadAllText(absolutePath);
        }
        if (assetType == typeof(Texture2D))
        {
            Texture2D texture2D = new Texture2D(1, 1);
            texture2D.LoadImage(File.ReadAllBytes(absolutePath));
            return texture2D;
        }
        throw new NotSupportedException();
    }

    private bool LoadCubism(string path)
    {
        bool flag = false;
        CubismModel cubismModel = null;
        try
        {
            CubismModel3Json cubismModel3Json = CubismModel3Json.LoadAtPath(path, new CubismModel3Json.LoadAssetAtPathHandler(BuiltinLoadAssetAtPath));
            if (cubismModel3Json != null)
            {
                cubismModel = cubismModel3Json.ToModel();
                flag = true;
            }
        }
        catch (Exception ex)
        {
            UnityEngine.Debug.LogError(ex.Message);
        }
        if (!flag)
        {
            UnityEngine.Debug.LogError("invalid json : " + path);
            this.RemoveCubismFolderFromFullPath(path);
            return false;
        }
        //this.avatarPool.AddCubism(cubismModel.gameObject);
        Animation animation = cubismModel.gameObject.AddComponent<Animation>();
        string path2 = Path.GetDirectoryName(path) + "/motions/";
        if (Directory.Exists(path2))
        {
            string[] files = Directory.GetFiles(path2, "*.json", SearchOption.AllDirectories);
            List<string> list = new List<string>();
            foreach (string path3 in files)
            {
                AnimationClip animationClip = CubismMotion3Json.LoadFrom(File.ReadAllText(path3)).ToAnimationClip();
                string text = Path.GetFileName(path3).Replace(".motion3.json", "");
                animationClip.name = text;
                list.Add(text);
                animationClip.wrapMode = WrapMode.Default;
                animationClip.legacy = true;
                animation.AddClip(animationClip, text);
            }
            animation.Play();
            cubismModel.gameObject.AddComponent<CubismMotionController>().SetClipList(list);
        }
        cubismModel.GetComponent<CubismRenderController>().SortingMode = CubismSortingMode.BackToFrontOrder;
        //this.selectAvatarWindow.AddCubismButton(path, cubismModel.gameObject);
        cubismModel.gameObject.tag = "Avatar";
        cubismModel.gameObject.transform.eulerAngles = new Vector3(0f, 0f, 0f);
        cubismModel.gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);
        cubismModel.gameObject.transform.position = new Vector3(0f, 1.5f, 0f);
        cubismModel.gameObject.AddComponent<Live2DApplier>();
        return true;
    }
}
