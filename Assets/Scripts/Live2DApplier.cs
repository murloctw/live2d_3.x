﻿using System;
//using Leap;
//using Leap.Unity;
using Live2D.Cubism.Core;
using Live2D.Cubism.Framework;
using Live2D.Cubism.Framework.MouthMovement;
using UnityEngine;

namespace Mitene
{
    public class Live2DApplier : MonoBehaviour
    {
        public CubismModel cubismModel { get; private set; }

        public CubismEyeBlinkController cubismEyeBlinkController { get; private set; }

        public CubismMouthController cubismMouthController { get; private set; }

        //public AvatarControllWrapper avatarControllWrapper { get; private set; }

        //public AdjustAvatarWindow adjustAvatarWindow { get; private set; }

        //public OVRLipSyncToVRM lipSyncToVRM { get; private set; }

        public CubismParameter ParamAngleX { get; private set; }

        public CubismParameter ParamAngleY { get; private set; }

        public CubismParameter ParamAngleZ { get; private set; }

        public CubismParameter ParamEyeLOpen { get; private set; }

        public CubismParameter ParamEyeLSmile { get; private set; }

        public CubismParameter ParamEyeRSmile { get; private set; }

        public CubismParameter ParamEyeROpen { get; private set; }

        public CubismParameter ParamEyeBallX { get; private set; }

        public CubismParameter ParamEyeBallY { get; private set; }

        public CubismParameter ParamBrowLY { get; private set; }

        public CubismParameter ParamBrowRY { get; private set; }

        public CubismParameter ParamBrowLX { get; private set; }

        public CubismParameter ParamBrowRX { get; private set; }

        public CubismParameter ParamBrowLAngle { get; private set; }

        public CubismParameter ParamBrowRAngle { get; private set; }

        public CubismParameter ParamBrowLForm { get; private set; }

        public CubismParameter ParamBrowRForm { get; private set; }

        public CubismParameter ParamMouthForm { get; private set; }

        public CubismParameter ParamMouthOpenY { get; private set; }

        public CubismParameter ParamCheek { get; private set; }

        public CubismParameter ParamBodyAngleX { get; private set; }

        public CubismParameter ParamBodyAngleY { get; private set; }

        public CubismParameter ParamBodyAngleZ { get; private set; }

        public CubismParameter ParamBreath { get; private set; }

        public CubismParameter ParamHairFront { get; private set; }

        public CubismParameter ParamHairSide { get; private set; }

        public CubismParameter ParamHairBack { get; private set; }

        public CubismParameter ParamArmLA { get; private set; }

        public CubismParameter ParamArmLB { get; private set; }

        public CubismParameter ParamArmRA { get; private set; }

        public CubismParameter ParamArmRB { get; private set; }

        public CubismParameter ParamArmRX { get; private set; }

        public CubismParameter ParamArmRY { get; private set; }

        public CubismParameter ParamArmLX { get; private set; }

        public CubismParameter ParamArmLY { get; private set; }

        private float T { get; set; }

        private float LastValue { get; set; }

        public void Reset()
        {
            this.T = 0f;
        }

        public float timeCounterForBlink { get; set; }

        private void Start()
        {
            this.timeCounterForBlink = 0f;
            //this.faceTrackingSwitch = UnityEngine.Object.FindObjectOfType<FaceTrackingSwitch>();
            //this.avatarControllWrapper = UnityEngine.Object.FindObjectOfType<AvatarControllWrapper>();
            //this.controllLive2DbyVR = UnityEngine.Object.FindObjectOfType<ControllLive2DbyVR>();
            this.cubismModel = this.FindCubismModel(false);
            //this.adjustAvatarWindow = UnityEngine.Object.FindObjectOfType<AdjustAvatarWindow>();
            //this.lipSyncToVRM = UnityEngine.Object.FindObjectOfType<OVRLipSyncToVRM>();
            try
            {
                //this.leapServiceProvider = UnityEngine.Object.FindObjectOfType<LeapServiceProvider>();
                //this.leapHandController = this.leapServiceProvider.transform;
                //this.leapController = this.leapServiceProvider.GetLeapController();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError("Live2DApplier::Start() LeapMotion Error.\n" + ex.Message);
            }
            this.ParamAngleX = this.cubismModel.Parameters.FindById("ParamAngleX");
            if (this.ParamAngleX == null)
            {
                this.ParamAngleX = this.cubismModel.Parameters.FindById("PARAM_ANGLE_X");
            }
            this.ParamAngleY = this.cubismModel.Parameters.FindById("ParamAngleY");
            if (this.ParamAngleY == null)
            {
                this.ParamAngleY = this.cubismModel.Parameters.FindById("PARAM_ANGLE_Y");
            }
            this.ParamAngleZ = this.cubismModel.Parameters.FindById("ParamAngleZ");
            if (this.ParamAngleZ == null)
            {
                this.ParamAngleZ = this.cubismModel.Parameters.FindById("PARAM_ANGLE_Z");
            }
            this.ParamEyeLOpen = this.cubismModel.Parameters.FindById("ParamEyeLOpen");
            if (this.ParamEyeLOpen == null)
            {
                this.ParamEyeLOpen = this.cubismModel.Parameters.FindById("PARAM_EYE_L_OPEN");
            }
            this.ParamEyeROpen = this.cubismModel.Parameters.FindById("ParamEyeROpen");
            if (this.ParamEyeROpen == null)
            {
                this.ParamEyeROpen = this.cubismModel.Parameters.FindById("PARAM_EYE_R_OPEN");
            }
            this.ParamEyeBallX = this.cubismModel.Parameters.FindById("ParamEyeBallX");
            if (this.ParamEyeBallX == null)
            {
                this.ParamEyeBallX = this.cubismModel.Parameters.FindById("PARAM_EYE_BALL_X");
            }
            this.ParamEyeBallY = this.cubismModel.Parameters.FindById("ParamEyeBallY");
            if (this.ParamEyeBallY == null)
            {
                this.ParamEyeBallY = this.cubismModel.Parameters.FindById("PARAM_EYE_BALL_Y");
            }
            this.ParamBrowLY = this.cubismModel.Parameters.FindById("ParamBrowLY");
            if (this.ParamBrowLY == null)
            {
                this.ParamBrowLY = this.cubismModel.Parameters.FindById("PARAM_BROW_L_Y");
            }
            this.ParamBrowRY = this.cubismModel.Parameters.FindById("ParamBrowRY");
            if (this.ParamBrowRY == null)
            {
                this.ParamBrowRY = this.cubismModel.Parameters.FindById("PARAM_BROW_R_Y");
            }
            this.ParamMouthOpenY = this.cubismModel.Parameters.FindById("ParamMouthOpenY");
            if (this.ParamMouthOpenY == null)
            {
                this.ParamMouthOpenY = this.cubismModel.Parameters.FindById("PARAM_MOUTH_OPEN_Y");
            }
            this.ParamBodyAngleX = this.cubismModel.Parameters.FindById("ParamBodyAngleX");
            if (this.ParamBodyAngleX == null)
            {
                this.ParamBodyAngleX = this.cubismModel.Parameters.FindById("PARAM_BODY_ANGLE_X");
            }
            this.ParamBodyAngleZ = this.cubismModel.Parameters.FindById("ParamBodyAngleZ");
            if (this.ParamBodyAngleZ == null)
            {
                this.ParamBodyAngleZ = this.cubismModel.Parameters.FindById("PARAM_BODY_ANGLE_Z");
            }
            this.ParamArmLA = this.cubismModel.Parameters.FindById("ParamArmLA");
            this.ParamArmLB = this.cubismModel.Parameters.FindById("ParamArmLB");
            this.ParamArmRA = this.cubismModel.Parameters.FindById("ParamArmRA");
            this.ParamArmRB = this.cubismModel.Parameters.FindById("ParamArmRB");
            this.ParamArmRX = this.cubismModel.Parameters.FindById("ParamArmRX");
            this.ParamArmRY = this.cubismModel.Parameters.FindById("ParamArmRY");
            this.ParamArmLX = this.cubismModel.Parameters.FindById("ParamArmLX");
            this.ParamArmLY = this.cubismModel.Parameters.FindById("ParamArmLY");
            if (this.ParamEyeLOpen != null)
            {
                this.EyeOpenValue = this.ParamEyeLOpen.Value;
            }
        }

        public void DoGaze(Live2DApplier.Gaze gaze, float speed)
        {
            this.MoveEyeSpeed = speed;
            switch (gaze)
            {
                case Live2DApplier.Gaze.center:
                    this.x = 0f;
                    this.y = 0f;
                    return;
                case Live2DApplier.Gaze.left:
                    this.x = 1f;
                    this.y = 0f;
                    return;
                case Live2DApplier.Gaze.right:
                    this.x = -1f;
                    this.y = 0f;
                    return;
                case Live2DApplier.Gaze.up:
                    this.x = 0f;
                    this.y = 1f;
                    return;
                case Live2DApplier.Gaze.down:
                    this.x = 0f;
                    this.y = -1f;
                    return;
                default:
                    return;
            }
        }

        public void DoArmMotion(Live2DApplier.ArmMotion armMotion)
        {
            switch (armMotion)
            {
                case Live2DApplier.ArmMotion.up_out_right:
                    this.armRX = -30f;
                    this.armRY = 30f;
                    return;
                case Live2DApplier.ArmMotion.up_in_right:
                    this.armRX = 30f;
                    this.armRY = 30f;
                    return;
                case Live2DApplier.ArmMotion.down_out_right:
                    this.armRX = -30f;
                    this.armRY = -30f;
                    return;
                case Live2DApplier.ArmMotion.down_in_right:
                    this.armRX = 30f;
                    this.armRY = -30f;
                    return;
                case Live2DApplier.ArmMotion.up_out_left:
                    this.armLX = -30f;
                    this.armLY = 30f;
                    return;
                case Live2DApplier.ArmMotion.up_in_left:
                    this.armLX = 30f;
                    this.armLY = 30f;
                    return;
                case Live2DApplier.ArmMotion.down_out_left:
                    this.armLX = -30f;
                    this.armLY = -30f;
                    return;
                case Live2DApplier.ArmMotion.down_in_left:
                    this.armLX = 30f;
                    this.armLY = -30f;
                    return;
                default:
                    return;
            }
        }

        private void LateUpdate()
        {
            if (this.ParamEyeBallX && this.ParamEyeBallY)
            {
                this.ParamEyeBallX.Value = Mathf.SmoothDamp(this.ParamEyeBallX.Value, this.x, ref this.eyeBallXvelocity, this.MoveEyeSpeed);
                this.ParamEyeBallY.Value = Mathf.SmoothDamp(this.ParamEyeBallY.Value, this.y, ref this.eyeBallYvelocity, this.MoveEyeSpeed);
            }
            //if (this.avatarControllWrapper.nowOperateMode == AvatarControllWrapper.OperationMode.FaceTracking && this.ParamArmRX && this.ParamArmRY && this.ParamArmLX && this.ParamArmLY)
            //{
            //    this.ParamArmRX.Value = Mathf.SmoothDamp(this.ParamArmRX.Value, this.armRX, ref this.ArmRXvelocity, 0.15f);
            //    this.ParamArmRY.Value = Mathf.SmoothDamp(this.ParamArmRY.Value, this.armRY, ref this.ArmRYvelocity, 0.15f);
            //    this.ParamArmLX.Value = Mathf.SmoothDamp(this.ParamArmLX.Value, this.armLX, ref this.ArmLXvelocity, 0.15f);
            //    this.ParamArmLY.Value = Mathf.SmoothDamp(this.ParamArmLY.Value, this.armLY, ref this.ArmLYvelocity, 0.15f);
            //}
            //switch (this.avatarControllWrapper.nowOperateMode)
            //{
            //    case AvatarControllWrapper.OperationMode.FaceTrackingAndLeapMotion:
            //        this.ApplyFingerMotion();
            //        break;
            //}
            //FaceTrackingBase currentClickFaceTracking = this.faceTrackingSwitch.GetCurrentClickFaceTracking();
            //if (this.adjustAvatarWindow.blinkActionType == AdjustAvatarWindow.BlinkActionType.parameter && currentClickFaceTracking.IsUseFaceTracking() && this.CurrentPhase == Live2DApplier.Phase.Idling && this.ParamEyeLOpen != null && this.ParamEyeROpen != null)
            //{
            //    this.ParamEyeLOpen.Value = currentClickFaceTracking.ParamEyeLOpen;
            //    this.ParamEyeROpen.Value = currentClickFaceTracking.ParamEyeROpen;
            //}
            //if (this.ParamBodyAngleZ == null)
            //{
            //    return;
            //}
            //if (currentClickFaceTracking.IsUseFaceTracking() || currentClickFaceTracking.isResetting)
            //{
            //    AvatarControllWrapper.ProjectionMethod projectionMethod = this.avatarControllWrapper.projectionMethod;
            //    if (projectionMethod != AvatarControllWrapper.ProjectionMethod.MirrorImage)
            //    {
            //        if (projectionMethod == AvatarControllWrapper.ProjectionMethod.RealImage)
            //        {
            //            if (this.ParamBodyAngleX != null)
            //            {
            //                this.ParamBodyAngleX.BlendToValue(CubismParameterBlendMode.Override, -currentClickFaceTracking.ParamBodyAngleX);
            //            }
            //            if (this.ParamBodyAngleZ != null)
            //            {
            //                this.ParamBodyAngleZ.BlendToValue(CubismParameterBlendMode.Override, -currentClickFaceTracking.ParamBodyAngleZ);
            //            }
            //            if (this.ParamAngleX != null)
            //            {
            //                this.ParamAngleX.BlendToValue(CubismParameterBlendMode.Override, -currentClickFaceTracking.ParamAngleX);
            //            }
            //            if (this.ParamAngleY != null)
            //            {
            //                this.ParamAngleY.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamAngleY);
            //            }
            //            if (this.ParamAngleZ != null)
            //            {
            //                this.ParamAngleZ.BlendToValue(CubismParameterBlendMode.Override, -currentClickFaceTracking.ParamAngleZ);
            //            }
            //            if (this.ParamBrowLY != null)
            //            {
            //                this.ParamBrowLY.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamBrowRY);
            //            }
            //            if (this.ParamBrowRY != null)
            //            {
            //                this.ParamBrowRY.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamBrowLY);
            //            }
            //            if (this.ParamEyeBallX != null)
            //            {
            //                this.ParamEyeBallX.Value = Mathf.SmoothDamp(this.ParamEyeBallX.Value, -currentClickFaceTracking.ParamEyeBallX, ref this.eyeBallXvelocity, 0.5f);
            //            }
            //            if (this.ParamEyeBallY != null)
            //            {
            //                this.ParamEyeBallY.Value = Mathf.SmoothDamp(this.ParamEyeBallY.Value, -currentClickFaceTracking.ParamEyeBallY, ref this.eyeBallYvelocity, 0.5f);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        if (this.ParamBodyAngleX != null)
            //        {
            //            this.ParamBodyAngleX.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamBodyAngleX);
            //        }
            //        if (this.ParamBodyAngleZ != null)
            //        {
            //            this.ParamBodyAngleZ.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamBodyAngleZ);
            //        }
            //        if (this.ParamAngleX != null)
            //        {
            //            this.ParamAngleX.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamAngleX);
            //        }
            //        if (this.ParamAngleY != null)
            //        {
            //            this.ParamAngleY.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamAngleY);
            //        }
            //        if (this.ParamAngleZ != null)
            //        {
            //            this.ParamAngleZ.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamAngleZ);
            //        }
            //        if (this.ParamBrowLY != null)
            //        {
            //            this.ParamBrowLY.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamBrowLY);
            //        }
            //        if (this.ParamBrowRY != null)
            //        {
            //            this.ParamBrowRY.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamBrowRY);
            //        }
            //        if (this.ParamEyeBallX != null)
            //        {
            //            this.ParamEyeBallX.Value = Mathf.SmoothDamp(this.ParamEyeBallX.Value, currentClickFaceTracking.ParamEyeBallX, ref this.eyeBallXvelocity, 0.5f);
            //        }
            //        if (this.ParamEyeBallY != null)
            //        {
            //            this.ParamEyeBallY.Value = Mathf.SmoothDamp(this.ParamEyeBallY.Value, -currentClickFaceTracking.ParamEyeBallY, ref this.eyeBallYvelocity, 0.5f);
            //        }
            //    }
            //}
            //if (this.adjustAvatarWindow.isEnableLipSync)
            //{
            //    AdjustAvatarWindow.LipSyncType lipSyncType = this.adjustAvatarWindow.lipSyncType;
            //    if (lipSyncType != AdjustAvatarWindow.LipSyncType.FaceRecognition)
            //    {
            //        if (lipSyncType == AdjustAvatarWindow.LipSyncType.AudioRecognition)
            //        {
            //            if (this.ParamMouthOpenY != null)
            //            {
            //                this.ParamMouthOpenY.BlendToValue(CubismParameterBlendMode.Override, this.lipSyncToVRM.A);
            //            }
            //        }
            //    }
            //    else if (this.ParamMouthOpenY != null)
            //    {
            //        this.ParamMouthOpenY.BlendToValue(CubismParameterBlendMode.Override, currentClickFaceTracking.ParamMouthOpenY);
            //    }
            //}
            if (this.CurrentPhase != Live2DApplier.Phase.Idling)
            {
                if (this.ParamEyeLOpen != null)
                {
                    this.ParamEyeLOpen.BlendToValue(CubismParameterBlendMode.Override, this.previousParamEyeLOpen);
                }
                if (this.ParamEyeROpen != null)
                {
                    this.ParamEyeROpen.BlendToValue(CubismParameterBlendMode.Override, this.previousParamEyeROpen);
                }
                if (this.CurrentPhase == Live2DApplier.Phase.Blinking_Last)
                {
                    this.CurrentPhase = Live2DApplier.Phase.Idling;
                }
            }
        }

        private void FixedUpdate()
        {
            //if (this.ParamEyeLOpen != null && this.ParamEyeROpen != null)
            //{
            //    FaceTrackingBase currentClickFaceTracking = this.faceTrackingSwitch.GetCurrentClickFaceTracking();
            //    if (this.adjustAvatarWindow.blinkActionType == AdjustAvatarWindow.BlinkActionType.animation && currentClickFaceTracking.isRequestEyeBlink && this.CurrentPhase == Live2DApplier.Phase.Idling && base.gameObject.activeInHierarchy)
            //    {
            //        this.CurrentPhase = Live2DApplier.Phase.Blinking_Close;
            //        this.previousParamEyeLOpen = this.ParamEyeLOpen.Value;
            //        this.previousParamEyeROpen = this.ParamEyeROpen.Value;
            //        this.adjustmentEyeOpenValue = (int)(this.EyeOpenValue * 5f);
            //        this.blinkState = 0;
            //    }
            //    if (this.avatarControllWrapper.enableAutoBlink)
            //    {
            //        this.timeCounterForBlink += Time.deltaTime;
            //        if (this.timeCounterForBlink >= this.avatarControllWrapper.blinkInterval)
            //        {
            //            this.timeCounterForBlink = 0f;
            //            if (this.CurrentPhase == Live2DApplier.Phase.Idling && base.gameObject.activeInHierarchy)
            //            {
            //                this.CurrentPhase = Live2DApplier.Phase.Blinking_Close;
            //                this.previousParamEyeLOpen = this.ParamEyeLOpen.Value;
            //                this.previousParamEyeROpen = this.ParamEyeROpen.Value;
            //                this.adjustmentEyeOpenValue = (int)(this.EyeOpenValue * 5f);
            //                this.blinkState = 0;
            //            }
            //        }
            //    }
            //    if (this.CurrentPhase != Live2DApplier.Phase.Idling)
            //    {
            //        if (this.CurrentPhase == Live2DApplier.Phase.Blinking_Close)
            //        {
            //            this.blinkState++;
            //            this.previousParamEyeLOpen -= (float)this.blinkState * 0.2f;
            //            this.previousParamEyeROpen -= (float)this.blinkState * 0.2f;
            //            if (this.blinkState > this.adjustmentEyeOpenValue)
            //            {
            //                this.CurrentPhase = Live2DApplier.Phase.Blinking_Open;
            //                this.blinkState = 0;
            //                return;
            //            }
            //        }
            //        else if (this.CurrentPhase == Live2DApplier.Phase.Blinking_Open)
            //        {
            //            this.blinkState++;
            //            if (this.blinkState >= this.adjustmentEyeOpenValue)
            //            {
            //                this.CurrentPhase = Live2DApplier.Phase.Blinking_Last;
            //            }
            //            this.previousParamEyeLOpen = (float)this.blinkState * 0.2f;
            //            this.previousParamEyeROpen = (float)this.blinkState * 0.2f;
            //        }
            //    }
            //}
        }

        private void OnDisable()
        {
            this.blinkState = 0;
            this.CurrentPhase = Live2DApplier.Phase.Idling;
        }

        private void ApplyFingerMotion()
        {
            //bool flag = false;
            //if (this.leapController != null)
            //{
            //    flag = this.leapController.IsConnected;
            //}
            //if (flag)
            //{
            //    foreach (Hand hand in this.leapController.Frame(0).Hands)
            //    {
            //        if (hand.IsLeft)
            //        {
            //            Vector3 vector = hand.PalmPosition.ToVector3() - this.leapHandController.position;
            //            float t = Mathf.InverseLerp(0f, -350f, vector.x);
            //            float t2 = Mathf.InverseLerp(100f, 600f, vector.y);
            //            if (this.ParamArmRX != null && this.ParamArmRY != null && this.ParamArmLX != null && this.ParamArmLY != null)
            //            {
            //                AvatarControllWrapper.ProjectionMethod projectionMethod = this.avatarControllWrapper.projectionMethod;
            //                if (projectionMethod != AvatarControllWrapper.ProjectionMethod.MirrorImage)
            //                {
            //                    if (projectionMethod == AvatarControllWrapper.ProjectionMethod.RealImage)
            //                    {
            //                        this.ParamArmLX.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(30f, -30f, t));
            //                        this.ParamArmLY.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-30f, 30f, t2));
            //                    }
            //                }
            //                else
            //                {
            //                    this.ParamArmRX.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(30f, -30f, t));
            //                    this.ParamArmRY.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-30f, 30f, t2));
            //                }
            //            }
            //            else if (this.ParamArmLA != null && this.ParamArmRA != null)
            //            {
            //                AvatarControllWrapper.ProjectionMethod projectionMethod = this.avatarControllWrapper.projectionMethod;
            //                if (projectionMethod != AvatarControllWrapper.ProjectionMethod.MirrorImage)
            //                {
            //                    if (projectionMethod == AvatarControllWrapper.ProjectionMethod.RealImage)
            //                    {
            //                        this.ParamArmLA.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-10f, 10f, t));
            //                    }
            //                }
            //                else
            //                {
            //                    this.ParamArmRA.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-10f, 10f, t));
            //                }
            //            }
            //        }
            //        if (hand.IsRight)
            //        {
            //            Vector3 vector2 = hand.PalmPosition.ToVector3() - this.leapHandController.position;
            //            float t3 = Mathf.InverseLerp(0f, 350f, vector2.x);
            //            float t4 = Mathf.InverseLerp(100f, 600f, vector2.y);
            //            if (this.ParamArmRX != null && this.ParamArmRY != null && this.ParamArmLX != null && this.ParamArmLY != null)
            //            {
            //                AvatarControllWrapper.ProjectionMethod projectionMethod = this.avatarControllWrapper.projectionMethod;
            //                if (projectionMethod != AvatarControllWrapper.ProjectionMethod.MirrorImage)
            //                {
            //                    if (projectionMethod == AvatarControllWrapper.ProjectionMethod.RealImage)
            //                    {
            //                        this.ParamArmRX.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(30f, -30f, t3));
            //                        this.ParamArmRY.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-30f, 30f, t4));
            //                    }
            //                }
            //                else
            //                {
            //                    this.ParamArmLX.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(30f, -30f, t3));
            //                    this.ParamArmLY.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-30f, 30f, t4));
            //                }
            //            }
            //            else if (this.ParamArmLA != null && this.ParamArmRA != null)
            //            {
            //                AvatarControllWrapper.ProjectionMethod projectionMethod = this.avatarControllWrapper.projectionMethod;
            //                if (projectionMethod != AvatarControllWrapper.ProjectionMethod.MirrorImage)
            //                {
            //                    if (projectionMethod == AvatarControllWrapper.ProjectionMethod.RealImage)
            //                    {
            //                        this.ParamArmRA.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-10f, 10f, t3));
            //                    }
            //                }
            //                else
            //                {
            //                    this.ParamArmLA.BlendToValue(CubismParameterBlendMode.Override, Mathf.Lerp(-10f, 10f, t3));
            //                }
            //            }
            //        }
            //    }
            //}
        }

        //private FaceTrackingSwitch faceTrackingSwitch;

        private float EyeOpenValue;

        private Live2DApplier.Phase CurrentPhase;

        [SerializeField]
        [Range(1f, 10f)]
        public float Mean = 2.5f;

        [SerializeField]
        [Range(0.5f, 5f)]
        public float MaximumDeviation = 2f;

        [SerializeField]
        [Range(1f, 20f)]
        public float Timescale = 10f;

        //private ControllLive2DbyVR controllLive2DbyVR;

        private Transform leapHandController;

        //private LeapServiceProvider leapServiceProvider;

        //private Controller leapController;

        private float x;

        private float y;

        private float armRX = -30f;

        private float armRY = -30f;

        private float armLX = -30f;

        private float armLY = -30f;

        private float MoveEyeSpeed = 1f;

        private float eyeBallXvelocity;

        private float eyeBallYvelocity;

        private float ArmRXvelocity;

        private float ArmRYvelocity;

        private float ArmLXvelocity;

        private float ArmLYvelocity;

        private float previousParamEyeLOpen;

        private float previousParamEyeROpen;

        private int adjustmentEyeOpenValue;

        private int blinkState;

        public enum Gaze
        {
            center,
            left,
            right,
            up,
            down
        }

        public enum ArmMotion
        {
            up_out_right,
            up_in_right,
            down_out_right,
            down_in_right,
            up_out_left,
            up_in_left,
            down_out_left,
            down_in_left
        }

        private enum Phase
        {
            Idling,
            Blinking_Close,
            Blinking_Open,
            Blinking_Last
        }
    }
}
